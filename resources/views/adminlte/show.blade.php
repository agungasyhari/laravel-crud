@extends('adminlte.master')

@section('content')
<section class="content">

    <!-- Default box -->
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Detail</h3>

        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
            <i class="fas fa-minus"></i></button>
          <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fas fa-times"></i></button>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-12 col-md-12 col-lg-8 order-2 order-md-1">
            <div class="row">
                <div class="info-box">
                  <div class="info-box-content">
                    <span class="info-box-text text-left text-muted">Judul</span>
                    <span class="info-box-number text-left text-muted mb-0">{{ $pertanyaan->judul }} </span>
                  </div>
                </div>
              </div>
                <div class="info-box">
                  <div class="info-box-content">
                    <span class="info-box-text text-left text-muted">Pertanyaan</span>
                    <span class="info-box-number text-left text-muted mb-0">{{ $pertanyaan->isi }}</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->

  </section>
<h4>{{ $pertanyaan->judul }} </h4>
<p> {{ $pertanyaan->isi }}</p>
@endsection