@extends('adminlte.master')

@section('content')
<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Quick Example</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="/pertanyaan" method="POST">
      @csrf
      <div class="card-body">
        <div class="form-group">
          <label for="judul">Judul</label>
          <input type="text" class="form-control" id="judul" name="judul" placeholder="Judul Pertanyaan">
        @error('judul')
        <div class="alert alert-danger">{{ $message }}</div>    
        @enderror
        </div>
        <div class="form-group">
          <label for="isi">Pertanyaan</label>
          <input type="text" class="form-control" id="isi" name="isi" placeholder="Isi Pertanyaan">
          @error('isi')
          <div class="alert alert-danger">{{ $message }}</div>    
          @enderror
        </div>
      </div>
      <!-- /.card-body -->

      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </form>
  </div>
@endsection