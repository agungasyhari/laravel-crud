@extends('adminlte.master')

@section('content')
<div class="card">
    <div class="card-header">
      <h3 class="card-title">Daftar Pertanyaan</h3>

      <div class="card-tools">
        <div class="input-group input-group-sm" style="width: 150px;">
          <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

          <div class="input-group-append">
            <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
          </div>
        </div>
      </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body table-responsive p-0">
        @if(session('berhasil'))
        <div class="alert alert-success ml-3 mr-3 mt-3 mb-3"> 
            {{ session('berhasil')}}
        </div>
        @endif
        <a class = "btn btn-primary ml-3 mt-3 mb-3" href="/pertanyaan/create"> Tanya </a>
      <table class="table table-hover text-nowrap">
        <thead>
          <tr>
            <th>ID</th>
            <th>Judul</th>
            <th>Pertanyaan</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
            @foreach ($pertanyaan as $key => $pertanyaan)
                <tr>
                    <td> {{ $key + 1 }} </td>
                    <td> {{ $pertanyaan -> judul }} </td>
                    <td> {{ $pertanyaan -> isi }} </td>
                    <td style="display: flex;">
                    <a href="/pertanyaan/{{$pertanyaan->id}}" class="btn btn-warning btn-sm"> detail</a>
                    <a href="/pertanyaan/{{$pertanyaan->id}}/edit" class="btn btn-default btn-sm"> edit</a>
                    <form action="/pertanyaan/{{$pertanyaan->id}}" method="post">
                      @csrf
                      @method('DELETE')
                     <input type="submit" value="delete" class="btn btn-danger btn-sm">
                    </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
      </table>
    </div>
    <!-- /.card-body -->
  </div>
@endsection