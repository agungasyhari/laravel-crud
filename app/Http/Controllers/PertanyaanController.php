<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
   public function index() {
       $pertanyaan = DB::table('pertanyaan')->get();
    //    dd($pertanyaan);
       return view('adminlte.pertanyaan', compact('pertanyaan')); 
   }

   public function create() {
       return view('adminlte.buatpertanyaan');
   }

   public function store(Request $request) {
    $request->validate([
        'judul' => 'required',
        'isi' => 'required'
        ]);
    $query = DB::table('pertanyaan')->insert([
        'judul' => $request['judul'], 
        'isi' => $request['isi']
        ]);
        return redirect('/pertanyaan')->with('berhasil', 'Pertanyaan Berhasil di Publish');
   }

   public function show($id) {
       $pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();
       return view('adminlte.show', compact('pertanyaan'));
   }

   public function edit($id) {
       $pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();
       return view ('adminlte.edit', compact('pertanyaan'));
   }

   public function update($id, Request $request) {
        $query = DB::table('pertanyaan')
                    ->where('id', $id)
                    ->update([
                       'judul' => $request['judul'],
                       'isi' => $request['isi'] 
                    ]);

        return redirect('/pertanyaan')->with('berhasil', 'Berhasil Update Pertanyaan');
   }

   public function destroy($id) {
       $query = DB::table('pertanyaan')->where('id', $id)->delete();
       return redirect('/pertanyaan')->with('berhasil', 'Pertanyaan Berhasil Di Hapus');
   }
}
